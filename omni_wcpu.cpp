#include <iostream>
#include <stdlib.h>
#include <libuvc/libuvc.h>
#include "libusb-1.0/libusb.h"
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <thread>
#include <deque>
#include <mutex>
#include <atomic>
#include <functional>
#include <string.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include "tick_meter.hpp"

#define frameHeight 400 
#define frameWidth 400 

::TickMeter tm2;

using namespace std;
using namespace cv;

std::vector <double> U_barr;
std::vector <double> V_barr;

int framecount = 0;
float gamma_val, contrast_val;
//-------------------------------------------Notch Filter-------------------------------------
void computeNotchFilter(Mat &im, int wid, int hei) //remove the vertical band noise 
{
cv::Mat a = cv::Mat::zeros(wid/2,hei/2,CV_16UC1);
cv::Mat b = cv::Mat::zeros(wid/2,hei/2,CV_16UC1);
cv::Mat c = cv::Mat::zeros(wid/2,hei/2,CV_16UC1);
cv::Mat d = cv::Mat::zeros(wid/2,hei/2,CV_16UC1);



for(int jj=0; jj<wid/2; jj++){

for(int kk=0; kk<hei/2; kk++){

a.at<uint16_t>(jj,kk)= im.at<uint16_t>(jj*2,kk*2);
b.at<uint16_t>(jj,kk)= im.at<uint16_t>(jj*2+1,kk*2+1);
c.at<uint16_t>(jj,kk)= im.at<uint16_t>(jj*2,kk*2+1);
d.at<uint16_t>(jj,kk)= im.at<uint16_t>(jj*2+1,kk*2);

} }


Mat op,op1,op2,op3;
op = a;
op1 = b;
op2 = c;
op3 = d;

/* dft */

    Mat planes[] = {Mat_<float>(op), Mat::zeros(op.size(), CV_32F)};
    Mat planes1[] = {Mat_<float>(op1), Mat::zeros(op1.size(), CV_32F)};
    Mat planes2[] = {Mat_<float>(op2), Mat::zeros(op2.size(), CV_32F)};
    Mat planes3[] = {Mat_<float>(op3), Mat::zeros(op3.size(), CV_32F)};

Mat complex, complex1, complex2, complex3;

    merge(planes, 2, complex);
    merge(planes1, 2, complex1);
    merge(planes2, 2, complex2);
    merge(planes3, 2, complex3);         // Add to the expanded another plane with zeros

    dft(complex, complex);  // fourier transform
    dft(complex1, complex1);
    dft(complex2, complex2);  // fourier transform
    dft(complex3, complex3);

    complex.at<Vec2f>(0,100)[0] = complex.at<Vec2f>(0,100)[0] * 0;

    complex1.at<Vec2f>(0,100)[0] = complex1.at<Vec2f>(0,100)[0] * 0;

    complex2.at<Vec2f>(0,100)[0] = complex2.at<Vec2f>(0,100)[0] * 0;

    complex3.at<Vec2f>(0,100)[0] = complex3.at<Vec2f>(0,100)[0] * 0;

    Mat df_op, df_op1, df_op2, df_op3;

    idft(complex, df_op, DFT_SCALE | DFT_REAL_OUTPUT);
    idft(complex1, df_op1, DFT_SCALE | DFT_REAL_OUTPUT);
    idft(complex2, df_op2, DFT_SCALE | DFT_REAL_OUTPUT);
    idft(complex3, df_op3, DFT_SCALE | DFT_REAL_OUTPUT);

    df_op.convertTo(a, CV_16UC1);
    df_op1.convertTo(b, CV_16UC1);
    df_op2.convertTo(c, CV_16UC1);
    df_op3.convertTo(d, CV_16UC1);


for(int jj=0; jj<wid/2; jj++){

for(int kk=0; kk<hei/2; kk++){

      im.at<uint16_t>(jj*2,kk*2) =a.at<uint16_t>(jj,kk);
      im.at<uint16_t>(jj*2+1,kk*2+1) = b.at<uint16_t>(jj,kk);
      im.at<uint16_t>(jj*2,kk*2+1) =c.at<uint16_t>(jj,kk);
      im.at<uint16_t>(jj*2+1,kk*2) = d.at<uint16_t>(jj,kk);
      }
    }

}
//-----------------------------------FPS---------------------------------------------
void computeFrameRate()
{
  tm2.stop();
  float time_per_frame = tm2.getTimeMilli()/tm2.getCounter(); //Time taken for one frame
  float fps = 0;
  if(time_per_frame!=0)
  {
    fps = 1000./time_per_frame;
  }

  if(framecount%600==0){ // reset every 10 secs the tm2 timer so that accumulated value is more or less correct
    tm2.reset();
  }
  cout<<fps<<endl;
  tm2.start();
  framecount++;
} 
//--------------------------------------White Balance----------------------------------
void ComputeWB(Mat &input)
{      //WB Kernel, multiply with the RGB image
       Mat outMat = (Mat_<double>(3,3) << 0.9867082188003693, 0.00786949622938217, -0.006489056008413811, -0.0004566851723320097, 0.9800058858325491, 0.00007516291945704828, 0.006969462180983009, 0.03892768988577712, 1.266226101367861);
       Mat channels[3];
       Mat im_out[3];
       split(input,channels);
       Mat im_temp;
       //R
       cv::addWeighted(channels[0], outMat.at<double> (2), channels[1], outMat.at<double> (1), 0, im_temp);
       cv::addWeighted(im_temp, 1, channels[2], outMat.at<double> (0), 0, im_out[2]);
       // G
       cv::addWeighted(channels[0], outMat.at<double> (5), channels[1], outMat.at<double> (4), 0, im_temp);
       cv::addWeighted(im_temp, 1, channels[2], outMat.at<double> (3), 0, im_out[1]);
       // B
       cv::addWeighted(channels[0], outMat.at<double> (8), channels[1], outMat.at<double> (7), 0, im_temp);
       cv::addWeighted(im_temp, 1, channels[2], outMat.at<double> (6), 0, im_out[0]);

       // Modified RGB image
       cv::merge(im_out, 3, input);
}
//-------------------------------------Contrast------------------------------------------
void contrast(Mat & input,float contrastval)
{
    multiply(input, Scalar(contrastval,contrastval,contrastval), input); //multiply with a constant contrast value
}
//-------------------------------------Gamma Correction  < 1 -> Bright   ----------    > 1 -> dark   ------------------------------
void gammaCorrection(Mat &img, float gamma)
{
    unsigned char lut[256];
    for (int i = 0; i < 256; i++)
    {
        lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), gamma) * 255.0f); //Look-up table, the values replace the values in image
    }

    Mat out = img.clone();

              MatIterator_<Vec3b> it, end;
              for (it = out.begin<Vec3b>(), end = out.end<Vec3b>(); it != end; it++)
              {

                  (*it)[0] = lut[((*it)[0])];
                  (*it)[1] = lut[((*it)[1])];
                  (*it)[2] = lut[((*it)[2])];
              }
           out.copyTo(img);   
    }
//---------------------------------------Full frame with patient details and video displayed from this section-------------------------------
void fullFrame(Mat &input)
{
  int value = 800;
	float a,b,c,d;   
  Mat output(1080,1920,CV_8UC3,Scalar(0,0,0));
	namedWindow("Video",WINDOW_NORMAL);
  setWindowProperty("Video",WND_PROP_FULLSCREEN,WINDOW_FULLSCREEN);
	putText(output,"Name  :",Point(50,200),FONT_HERSHEY_SIMPLEX,1,Scalar(255,255,255),1,8,false);
  putText(output,"Age   :",Point(50,250),FONT_HERSHEY_SIMPLEX,1,Scalar(255,255,255),1,8,false);
  putText(output,"Gender:",Point(50,300),FONT_HERSHEY_SIMPLEX,1,Scalar(255,255,255),1,8,false);
  resize(input,input,Size(value,value));   
  a=1080-value;
	b=a/2;
	c=1420-value;
	d=c/2;
	input.copyTo(output(Rect(499+d,b,value,value)));
  imshow("Video",output);
  waitKey(1);
}

void cb(uvc_frame_t *frame, void *ptr)
{
  if(frame->height*frame->width*2 != frame->data_bytes)
  {
    std::cerr<< std::endl << "data size mismatch : " << frame->height << "x" << frame->width << "x2 != " << frame->data_bytes;
  }

  int wid = frame->width;
  int hei = frame->height;

  int mask = 0x03FF;
  static unsigned short data16[frameWidth*frameHeight];
    unsigned char * bytedata = (unsigned char*) frame->data;

  for(int ii=0; ii < wid*hei; ii++) {
    data16[ii] = (unsigned short) (bytedata[2*ii]) | (unsigned short) (bytedata[2*ii+1] << 8) ;
    data16[ii]= data16[ii] & mask;
  }
  Mat bayer(hei,wid,CV_16UC1,(void*) data16);
  cv::Mat bgrmat(wid,hei,CV_16UC3);
  computeNotchFilter(bayer,wid,hei);
  cvtColor(bayer, bgrmat, CV_BayerRG2BGR);
  bgrmat.convertTo(bgrmat,CV_8UC3);
  computeFrameRate();
  ComputeWB(bgrmat);
  contrast(bgrmat,contrast_val);
  gammaCorrection(bgrmat,gamma_val);
  fullFrame(bgrmat);
}

int main(int argc,char **argv)
{
    uvc_context_t *ctx; 
    uvc_error_t res;
    uvc_device_t *dev; 
    uvc_stream_ctrl_t ctrl;
    uvc_device_handle_t *devh;
    int result = 0;
    void *data16;
    string gamma_s, contrast_s;
    gamma_s = argv[1];
    contrast_s = argv[2];
    gamma_val = ::atof(gamma_s.c_str());
    contrast_val = ::atof(contrast_s.c_str());
    
    res = uvc_init(&ctx,NULL);

    if(res < 0)
    {
      uvc_perror(res,"Init_error\n");
      result = res;
    }

    res = uvc_find_device(ctx,&dev,1204,249,NULL);

    if(res < 0)
    {
      uvc_perror(res,"Device not found\n");
      result = 1;
    }

    res = uvc_open(dev,&devh);

    if(res < 0)
    {
      uvc_perror(res,"UVC open error\n");
      result = 2;
    }

    res = uvc_get_stream_ctrl_format_size(devh, &ctrl , UVC_FRAME_FORMAT_ANY, 400, 400, 30);
    uvc_print_stream_ctrl(&ctrl, stderr);

    if (res < 0)
    {
        uvc_perror(res, "uvc_get_stream_ctrl_format_size error\n");
        result= 3;
    }
    int dummy = 0; 

    res = uvc_start_streaming(devh, &ctrl, cb, &dummy, 0);

    if (res < 0)
    {
        uvc_perror(res, "uvc_start_streaming error\n");
        result= 4;
    }
    else
    {
    while(1)
    {
      
    }
    cerr<<" result: " << result << std::endl;

    if(devh==NULL)
    {
        std::cerr << "exiting\n";
        return result;
    }


    if(devh !=NULL)
    {
        std::cout <<"Stopped streaming"<< std::endl;
        uvc_stop_streaming(devh);
        puts("\nStopped streaming.");
        uvc_close(devh);
        puts("\nDevice closed");
    }
	}
    if(dev !=NULL)
    {
        std::cout <<"Device unref"<< std::endl;
        uvc_unref_device(dev);
        puts("\nDevice unref");
    }

    if(ctx!=NULL)
    {
        std::cout <<"UVC exited"<< std::endl;
        uvc_exit(ctx);
        puts("\nUVC exited");
    }

return 0;
}
