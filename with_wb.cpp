#include <iostream>
#include <stdlib.h>
#include <libuvc/libuvc.h>
#include "libusb-1.0/libusb.h"
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <thread>
#include <deque>
#include <mutex>
#include <atomic>
#include <functional>
#include <string.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include "tick_meter.hpp"

#define frameHeight 400 
#define frameWidth 400 


using namespace std;
using namespace cv;

std::vector <double> U_barr;
std::vector <double> V_barr;
float gamma_val, contrast_val;

void ComputeBGRGainMod(int framecount, Mat &input, Mat &output)
{
     Mat im_orig;
     double b = 0.001; 
     im_orig = input;
     Mat mat1, mat2, mat3, mat4, mat5, mat6, mat7, mat8, mat9, mat10, mat11, mat12, mat13, mat14, mat15, matrix;
     cv::Mat xfm_cat02 = (cv::Mat_<double>(3,3) << 0.7328, 0.4296, -.1624, -.7036, 1.6975, 0.0061, 0.0030, 0.0136, 0.9834);
     cv::Mat sRGBtoXYZ = (cv::Mat_<double>(3,3) << 0.4124564, 0.3575761, 0.1804375, 0.2126729, 0.7151522, 0.0721750, 0.0193339, 0.1191920, 0.9503041);
     cv::Mat inv_sRGBtoXYZ = sRGBtoXYZ.clone().inv();
     cv::Mat imRGB;
     imRGB.convertTo(imRGB,CV_32F);
     im_orig.convertTo(im_orig,CV_32F);
     for (int iter=1; iter<=15; iter++)
    {
        cv::Mat im_RGB[3];
        cv::split(im_orig.clone(),im_RGB);

        //cv::split(im_orig.clone(),im_RGB11);
       // std::cout <<"after split" << std::endl;
        cv::Mat im_temp;
        im_temp.convertTo(im_temp,CV_32F);
        cv::Mat im_out[3];

        im_out[0].convertTo(im_out[0],CV_32F);
        im_out[1].convertTo(im_out[1],CV_32F);
        im_out[2].convertTo(im_out[2],CV_32F);


        // Input Frame Modification with Gain Values
        cv::Mat outMat;
        outMat.convertTo(outMat,CV_32F);
        cv::Mat d_gain;
        d_gain.convertTo(d_gain,CV_32F);

        // ------------------- Color conversion BGR to YUV --------------------
        cv::Mat img_cpu1(400,400,CV_32F);
        img_cpu1 = im_orig;
       
         // ---------------------------    Y Component    ------------------------
        cv::Mat im_RGB1[3];
        
        im_RGB1[0].convertTo(im_RGB1[0],CV_32F);
        im_RGB1[1].convertTo(im_RGB1[1],CV_32F);
        im_RGB1[2].convertTo(im_RGB1[2],CV_32F);
        
        cv::split(img_cpu1.clone(),im_RGB1);
        cv::Mat im_RGB1_1(400,400,CV_32F);
        cv::Mat im_RGB2_1(400,400,CV_32F);
        cv::addWeighted(im_RGB1[0], 0.114, im_RGB1[1], 0.587, 0, im_RGB1_1);
        cv::addWeighted(im_RGB1_1, 1, im_RGB1[2], 0.299, 0, im_RGB2_1);
        cv::transpose(im_RGB2_1.clone(),im_RGB2_1);

        // ---------------------------    U Component    ------------------------
        cv::Mat im_RGB2[3];
        im_RGB2[0].convertTo(im_RGB2[0],CV_32F);
        im_RGB2[1].convertTo(im_RGB2[1],CV_32F);
        im_RGB2[2].convertTo(im_RGB2[2],CV_32F);
        cv::split(img_cpu1.clone(),im_RGB2);
        cv::Mat im_RGB1_2(400,400,CV_32F);
        cv::Mat im_RGB2_2(400,400,CV_32F);
        cv::addWeighted(im_RGB2[0], 0.886, im_RGB2[1], -0.587, 0, im_RGB1_2);
        cv::addWeighted(im_RGB1_2, 1, im_RGB2[2], -0.299, 0, im_RGB2_2);
        cv::transpose(im_RGB2_2.clone(),im_RGB2_2);
        

        // ---------------------------    V Component    -------------------------
        cv::Mat im_RGB3[3];
        im_RGB3[0].convertTo(im_RGB3[0],CV_32F);
        im_RGB3[1].convertTo(im_RGB3[1],CV_32F);
        im_RGB3[2].convertTo(im_RGB3[2],CV_32F);
        cv::split(img_cpu1.clone(),im_RGB3);
        cv::Mat im_RGB1_3(400,400,CV_32F);
        cv::Mat im_RGB2_3(400,400,CV_32F);
        cv::addWeighted(im_RGB3[0], -0.114, im_RGB3[1], -0.587, 0, im_RGB1_3);
        cv::addWeighted(im_RGB1_3, 1, im_RGB3[2], 0.701, 0, im_RGB2_3);
        cv::transpose(im_RGB2_3.clone(),im_RGB2_3);
        
        // --------------------- Gray Chromaticity   (F = (|U|+|V|)/Y)) -----------
        cv::Mat UV(400,400,CV_32F);
        cv::Mat F(400,400,CV_32F);
        cv::Mat U(400,400,CV_32F);
        cv::Mat V(400,400,CV_32F);
        U = cv::abs(im_RGB2_2.clone());
        V = cv::abs(im_RGB2_3.clone());
        cv::add(U,V,UV);
        cv::divide(UV,im_RGB2_1,F);
        //cout<<F<<endl;
        // ----------------- for all (F<T) in image find mean of U and V------------
        cv::Mat thr, UU, VV;
        thr.convertTo(thr,CV_32F);
        UU.convertTo(UU,CV_32F);
        VV.convertTo(VV,CV_32F);
        cv::threshold(F,thr,0.3,1,cv::THRESH_BINARY_INV);

        cv::Mat thrSum;
        thrSum.convertTo(thrSum,CV_32F);
        thrSum = cv::sum(thr);
        //cout<<F<<endl;
        cv::multiply(im_RGB2_2.clone(),thr,UU);
        cv::multiply(im_RGB2_3.clone(),thr,VV);
        cv::Mat U1, V1, U1_bar, V1_bar;
        U1.convertTo(U1,CV_32F);
        V1.convertTo(V1,CV_32F);
        U1_bar.convertTo(U1_bar,CV_32F);
        V1_bar.convertTo(V1_bar,CV_32F);
        U1 = cv::sum(UU);
        
        cv::divide(U1,thrSum,U1_bar);

        V1 = cv::sum(VV);
        cv::divide(V1,thrSum,V1_bar);
        //cout<<"\n\n"<< U <<"\n"<< V <<"\n"<<endl;
        cv::Mat U_bar1,V_bar1;
        U_bar1.convertTo(U_bar1,CV_32F);
        V_bar1.convertTo(V_bar1,CV_32F);
        U_bar1 = U1_bar;
        V_bar1 = V1_bar;
        


        double U_bar, V_bar;
        cv::minMaxLoc(U1_bar,NULL,&U_bar);
        cv::minMaxLoc(V1_bar,NULL,&V_bar);

        //..............................rgbEst......................................
        double rgbEst1 = (99.99999999999999+V_bar*0.9999999999999999);
        double rgbEst2 = (99.99999999999999+U_bar*-0.1942078364565588+V_bar*-0.5093696763202725);
        double rgbEst3 = (99.99999999999999+U_bar*0.9999999999999999);
        //cout<<rgbEst2<<endl;
        //.................... xyz = sRGBtoXYZ * rgbEst..............................
        double xyz1 = (rgbEst1*0.4124564+rgbEst2*0.3575761+rgbEst3*0.1804375);
        double xyz2 = (rgbEst1*0.2126729+rgbEst2*0.7151522+rgbEst3*0.072175);
        double xyz3 = (rgbEst1*0.0193339+rgbEst2*0.119192+rgbEst3*0.9503041);

        //..................... Calculate xy chromaticity............................
        double s = xyz1+xyz2+xyz3;
        double xy1 = xyz1/s;
        double xy2 = xyz2/s;

        //.................... Normalize Y to 100 so D65 luminance comparable........
        double xyzEst1 = 100/xy2*xy1;
        double xyzEst2 = 100;
        double xyzEst3 = 100/xy2*(1-xy1-xy2);

        //.................gain = ((xfm*xyz_target) ./ (xfm*xyz_est))................

        double B_Gainn = (94.9232)/(0.7328*xyzEst1+0.4296*xyzEst2+(-0.1624)*xyzEst3);
        double G_Gainn = (103.544)/((-0.7036)*xyzEst1+(1.6975)*xyzEst2+0.0061*xyzEst3);
        double R_Gainn = (108.718)/((0.0030)*xyzEst1+(0.0136)*xyzEst2+0.9834*xyzEst3);



        d_gain = (cv::Mat_<double>(3,3) << B_Gainn, 0, 0, 0, G_Gainn, 0, 0, 0, R_Gainn);
        cv::solve(xfm_cat02,(d_gain*xfm_cat02), outMat);
        outMat = inv_sRGBtoXYZ * outMat.clone() * sRGBtoXYZ;
        
        //cout<<outMat<<endl;
        if(iter==1){
            mat1 = outMat;
        }
        else if(iter==2){
            mat2 = outMat;
        }
        else if(iter==3){
            mat3 = outMat;
        }
        else if(iter==4){
            mat4 = outMat;
        }
        else if(iter==5){
            mat5 = outMat;
        }
        else if(iter==6){
            mat6 = outMat;
        }
        else if(iter==7){
            mat7 = outMat;
        }
        else if(iter==8){
            mat8 = outMat;
        }
        else if(iter==9){
            mat9 = outMat;
        }
        else if(iter==10){
            mat10 = outMat;
        }
        else if(iter==11){
            mat11 = outMat;
        }
        else if(iter==12){
            mat12 = outMat;
        }
        else if(iter==13){
            mat13 = outMat;
        }

        else if(iter==14){
            mat14 = outMat;
        }
        else{
            mat15 = outMat;
        }

        
       // R
       cv::addWeighted(im_RGB[0], outMat.at<double> (2), im_RGB[1], outMat.at<double> (1), 0, im_temp);
       cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (0), 0, im_out[2]);
       // G
       cv::addWeighted(im_RGB[0], outMat.at<double> (5), im_RGB[1], outMat.at<double> (4), 0, im_temp);
       cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (3), 0, im_out[1]);
       // B
       cv::addWeighted(im_RGB[0], outMat.at<double> (8), im_RGB[1], outMat.at<double> (7), 0, im_temp);
       cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (6), 0, im_out[0]);
       cout<<"Hello"<<endl;

       // Modified RGB image
       cv::merge(im_out, 3, imRGB);

       imRGB.clone().copyTo(im_orig);
       
 }
   matrix =  mat15 * mat14 * mat13 * mat12 * mat11 * mat10 * mat9 * mat8 * mat7 * mat6 * mat5 * mat4 * mat3 * mat2 * mat1;
   
   cv::Mat im_MAT[3];
   cv::split(input.clone(),im_MAT);
   cv::Mat im_temp;
   cv::Mat im_out[3];

   cv::addWeighted(im_MAT[0], matrix.at<double> (2), im_MAT[1], matrix.at<double> (1), 0, im_temp);
   cv::addWeighted(im_temp, 1, im_MAT[2], matrix.at<double> (0), 0, im_out[2]);
       // G
   cv::addWeighted(im_MAT[0], matrix.at<double> (5), im_MAT[1], matrix.at<double> (4), 0, im_temp);
   cv::addWeighted(im_temp, 1, im_MAT[2], matrix.at<double> (3), 0, im_out[1]);
       // B
   cv::addWeighted(im_MAT[0], matrix.at<double> (8), im_MAT[1], matrix.at<double> (7), 0, im_temp);
   cv::addWeighted(im_temp, 1, im_MAT[2], matrix.at<double> (6), 0, im_out[0]);
   
   cv::merge(im_out, 3, output);
}

void contrast(Mat & input,float contrastval)
{
    multiply(input, Scalar(contrastval,contrastval,contrastval), input);
}

void gammaCorrection(Mat &img, float gamma)
{
    unsigned char lut[256];
    for (int i = 0; i < 256; i++)
    {
        lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), gamma) * 255.0f);
    }

    Mat out = img.clone();

              MatIterator_<Vec3b> it, end;
              for (it = out.begin<Vec3b>(), end = out.end<Vec3b>(); it != end; it++)
              {

                  (*it)[0] = lut[((*it)[0])];
                  (*it)[1] = lut[((*it)[1])];
                  (*it)[2] = lut[((*it)[2])];
              }
           out.copyTo(img);   
    }
int main(int argc, char **argv)
{
    string gamma_s, contrast_s;
    gamma_s = argv[1];
    contrast_s = argv[2];
    Mat im(400,400,CV_32FC3);
    im = imread("/home/nandini/Desktop/Nivin_Omni/Nivin/stand_alone/wb_images/17.jpg");
    Mat out;
    //cout<<im<<endl;
    gamma_val = ::atof(gamma_s.c_str());
    contrast_val = ::atof(contrast_s.c_str());
    //cout<<"argc:"<<argc<<"\nargv:"<<gamma_val<<"\n   \t"<<contrast_val<<endl;
    ComputeBGRGainMod(15,im,out);
    //contrast(im,contrast_val);
    //gammaCorrection(im,gamma_val);
    
    namedWindow("wb",CV_WINDOW_AUTOSIZE);

    imshow("wb",im);
    waitKey(0);
}